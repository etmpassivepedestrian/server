var mongoose = require('mongoose');

var NodeSerialNumbers = new mongoose.Schema({
    serial: String,
    validated: { type: Boolean, default: false }  
});

mongoose.model('NodeSerialNumber', NodeSerialNumbers);
