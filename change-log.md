## Change Log (By Version)

### v0.2.0
* Web API routes now added to the server for database interaction
#### Specific file changes
* ./routes
    * added the route redirect for improper routes to return to home
    * added the route for /dashboard
* ./load
    * created a new file that has loads event data
    * new folder location containing all the default data for the database
* ./app.js
    * MODIFIED the static routes to accommodate the addition of Carl's website resources
### v0.2.1
* Web API routes have been test thoroughly and adding an Event and getting all events work.
* Schema for Events needed to be adjusted
* Issues were found when using 'unique' fields when loading and adding data. FIXED but would need to purge the entire database.
#### Specific File changes
* ./load/drop.js
    * No longer uses the Model.remove but is still usable if schema is consistent.
* ./load/events.js
    * Uses the Events schema found in ./models/Events.js
* ./bin/www/
    * changed port to 3000
* ./app.js
    * PRINTS sever errors to console (line 80)
* ./models/Events.js
    * Schema changed to reflect changes by team and front end.

### v0.3.0

* Added the static path for the obtainting the chart data from the server
* Changing the port number to port 80 instead of 3000 as we managed to fix linux permission when running the file
* Rewritten `drop.js` to properly disconnect and close the connection when it has finished when dropping the database.
* Added a sample chart data (.tsv) file to solution
* Automated testing code now included using mocha framework
* Automated testing for all the GET routes (including static routes)
* Adjusted POST requests to now normalise the timezone to UTC+0 because front-end couldn't resolve angular 2's timezone issue of applying a timezone when already in the correct timezone.
* Added the mocha to npm command runs -- call `npm mocha` to run the main test file.
* Adding my `.gitignore` file to git for other to use
* Database code is initialised outside the 'app.js' call (see ./bin/www code)
* Added new files:
    * test.js
    * init-database.js
    * debug-app.js
* Current iteration has passed all automated and manual tests

### v0.3.1

* Pre-Update before major update to the server.
* Added 'Node' (Our sensor) to the database store
* Added 'NodeSerialNumber' store to keep record of valid serial numbers but not used for now.
* Created new routes for Node data access
    * GET /api/nodes - retrieve all node registered nodes
    * POST /api/nodes - add a new node to the database.
* Added the Mocha testing framework for automated testing.

### v0.4.0 (Major Update)

* I am happy to announce many new features to the HTTP server in this new version upgrade. The server now supports data backup saves to the server and sending of backup data to the client - call it screenshots. There is no cap on the number of screen shots and further work should limit number documents saves to disk.
* Server is now capable of updating single event documents within the database. This is the first of our query code that is on line and read to use.
* Server now has a new admin route that holds data manipulation and access, as follows:
    * Queries
    * Database Save to File
    * Deleting all documents from the database.
    * Purging the database
    * Updating Events (should be moved to normal api)

* The _id issue has been fixed for the website, we can now send 'id' instead and set it as a string 
* Code clean as gone smooth, if we have most of our functionality to manipulate the database in used by ExpressJS routes then we can remove some the older scripts. Commented out code has also been remove.


Plans for next iteration:
	* Writing authentication code for admin routes and privilliged http routes
	* Writing the test case for POST request to the server, now that we have new remove and purge routes
	* Writing more query code to search event by name

### v0.4.2 (Don't know how v0.4.1 changes go missed)

* Updated Access Controls Headers to allow for PUT, DELETE requests from remote hosts.
* Added 'Angular 2 Hack job for time' to the PUT /api/events/:id to correctly format the time correctly.
* Also yeah changed the route for queries from /api/queries/events/:id --> /api/events/:id
* Clean up alot of the code for the /routes/queries.js that was in the comments or not longer needed.
* 'PUT /api/events/:id' can now check if a field is empty and remove it from the update, because angular 2 does things it's own way.
* it is now possible to keep a website instance with the server code and only update code that is related to the serve, but programming knowledge is need and an update to the .gitignore

### v0.4.3 Fixes, Updates, Attaching Nodes to the database

* Mistake in queries.js in saving Node data, was missig reference to variable `temp`
* Mistake in models/events.js for Event having incorrect reference to `ObjectId`
* Added a new route that queries the database for Nodes that can be added to Event.
    * GET '/events/:id/addNodesToEventList'
* You can now run with PORT argument.
    * Before running the server, type:
        * 'set PORT=3000'
    * The server will run on that port when you call `sudo npm start`
* Events model was updated, now has new field called `nodesAttached` that referneces Nodes Documents
* Nodes model was updated, now has new field called `checked` which takes Boolean values
* PUT '/events/:id' now includes validation code to check if incomign node ObjectIds are valid
* PUT '/events/:id' is deemed complete and has been tested (manually with POSTMAN)
* Renamed the server to 'http-server- ...' for clarity
* Update the GET '/events' to output the `nodesAttached` field
* Added skeleton code for uploading json data to the server in event of a backup

### v0.4.4 Updates and editing events further

* New changes include:
    * functionality that removes node/sensor association to an event
    * functionality that removes an event entirely from the database
* Removed code that unnecessarily was pushing nodes to event from incoming update when it's only checking if node ids' exist in the database.
* Updated the response msg for deleting an event

### v1.0.0 Final update for release

* Added the package dependancies from the website component of the system
* Added new custom header called 'offset' that used to offset the time zone from the incoming time
* Added 'offset' to the allowed CORS headers list
* Update the routes that take in time from the user to use this new header
* Removed 'x-powered-by' for sure this time.

## future dev
* Improvements to make:
    * recording HTTP request session to a log file
    * NOT renaming index.html to index.ejs
    * Changing the view directory
    * Keeping index.html in the root directory not the `/views`
    * Complete upload code
* Todo
    * Add routes for the api
    * finish Web API for event
    * automated testing for POST requests

