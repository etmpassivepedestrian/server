var fs = require('fs');
var path = require('path');

var express = require('express');
var routerOptions = {
    caseSensitive: true
}
var router = express.Router(routerOptions);

/** DATABASE MODELS */

var mongoose = require('mongoose');
var Posts = mongoose.model('Post');
var Events = mongoose.model('Event');
var Nodes = mongoose.model('Node');
// var NodeSerialNumbers = mongoose.model('NodeSerialNumber');
// var Elements = mongoose.model('Element');


router.delete('/purge', function(req, res, next){
    //Database functions
    var db = mongoose.connection.db;
    // Uncomment when 
    mongoose.connection.db.dropDatabase();
    console.log(db);
    //response to the client
    res.send("INFO:\t Database was dropped!");
});

router.delete('/remove/events', function(req, res, next){
    //database action handling
    Events
        .remove({})
        .then(() => res.send("Event Data removed!"))
        .catch((err) => {
            next(err);
        });
});
router.delete('/remove/nodes', function(req, res, next){
    //database action handling
    Nodes
        .remove({})
        .then((node) => console.log("Node Data removed!"))
        .catch((err) => {
            next(err);
        });
});
// router.get('/remove/posts', function(req, res, next){
//     //database action handling
//     Posts.remove({})
//     .then(() => console.log("Post Data removed!"))
//     .catch((err) => {
//         next(err);
//     });
// });


router.get('/save/events', function(req, res, next){
    var path = '/user';
    var currentDate = new Date(Date.now());
    // var body = new Object();
    var temp = [];

    Events
        .find({})
        .then( (data) => {
            data.forEach( (value, index) => {
                var o = {
                    // id: value._id.toString(),
                    name: value.name,
                    location: value.location,
                    startDate: value.startDate,
                    endDate: value.endDate,
                    organiser: value.organiser
                };
                temp.push(o);
            });
            
            console.log(temp);
            // res.json(temp);
            var body = new Object();
            body.data = temp;
            res.data = body;
        }) 
        .then( () => {
            console.log(path);
            fs.mkdir(__dirname + path, (err) => {
                if (err){
                    if (err.code === "EEXIST"){
                        console.log('Dir already exists!');
                        return;
                    }
                    return next(err);
                }
                console.log('INFO:\t file created under the name ', path);
            });
            var timestamp = (currentDate.toDateString() + '_' + currentDate.getTime()).replace(new RegExp(' ', 'g'), '_');
            var writeFile = path + '/' + timestamp + '_events.json';
            // body.data = data;
            // you must stringify the JSON otherwise you get the toString that your data variable is an object array
            fs.writeFile(__dirname + '/' + writeFile, JSON.stringify(res.data, null, 4), (err) => {
                if (err) {
                    return next(err);
                }
                console.log("INFO:\t Data was saved to '%s'", writeFile);
                // Because of race conditions you need to put res.download() within this callback
                // This produces the 'save as ...' context
                res.download(__dirname + '/'+ writeFile);
                // next();
            });
        })
        .catch((err) => {
            next(err);
        });
});

router.get('/save/nodes', function(req, res, next){
    var path = '/user';
    var currentDate = new Date(Date.now());
    var body = new Object();
    var temp = [];

    Nodes
        .find({})
        .then( (data) => {
            data.forEach( (value, index) => {
                var o = {
                    // id: value._id.toString(),
                    serial: value.serial
                };
                temp.push(o);
            });
            
            console.log(temp);
            // res.json(temp);
            var body = new Object();
            body.data = temp;
            res.data = body;
        }) 
        .then( () => {
            console.log(path);
            fs.mkdir(__dirname + path, (err) => {
                if (err){
                    if (err.code === "EEXIST"){
                        console.log('Dir already exists!');
                        return;
                    }
                    return next(err);
                }
                console.log('INFO:\t file created under the name ', path);
            });
            var timestamp = (currentDate.toDateString() + '_' + currentDate.getTime()).replace(new RegExp(' ', 'g'), '_');
            var writeFile = path + '/' + timestamp + '_nodes.json';
            // you must stringify the JSON otherwise you get the toString that your data variable is an object array
            fs.writeFile(__dirname + '/' + writeFile, JSON.stringify(res.data, null, 4), (err) => {
                if (err) {
                    return next(err);
                }
                console.log("INFO:\t Data was saved to '%s'", writeFile);
                // Because of race conditions you need to put res.download() within this callback
                // This produces the 'save as ...' context
                res.download(__dirname + '/'+ writeFile);
                // next();
            });
        })
        .catch((err) => {
            next(err);
        });
});
// router.get('/save/events/', function(req, res, next){
//     //Database query for all the data in events
//     res.send('Thank you for stealing our data!')
// });

router.post('upload/events', function(req, res, next) {
    req.body.data.forEach( (value, index) => {
        
    });
});

router.post('upload/nodes', function(req, res, next) {
    req.body.data.forEach( (value, index) => {
        var Events = new Events(value);

        Events.save()
        .then( () => console.log("Data Saved!") )
        .catch( (err) => {
            console.error(err);
            next(err);
        })
    });
});

module.exports = router;