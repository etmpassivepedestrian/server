var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// Code for database init is now outside app.js
// var mongoose = require('mongoose');

var routes = require('./routes/index');
var users = require('./routes/users');
var api = require('./routes/web');
var admin = require('./routes/admin');
var queries = require('./routes/queries');

var app = express();

//disables the header X-powered-by express for security reasons

app.disable('x-powered-by');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Offset");
  next();
});

//Static routes
//This will affect html access to js, images and css
app.use(express.static(path.join(__dirname, 'public')));
//new static joins for the website.
app.use('/node_modules', express.static(path.join(__dirname, 'node_modules')));
app.use('/app', express.static(path.join(__dirname, 'app')));
app.use(express.static(path.join(__dirname, 'chartdata')));
//app.use('/ng2-nvd3', express.static(path.join(__dirname, 'node_models/ng2-nvd3')));
// app.use('/charts', express.static('chartdata'));
// app.use(express.static(path.join(__dirname, '/systemjs.config.js')));


//Set the routing for the website
app.use('/', routes);
app.use('/users', users);
app.use('/api', api);
app.use('/api/admin', admin);
app.use('/api', queries);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
    //display onto console
    console.log(err);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
