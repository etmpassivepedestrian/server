var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');

/** We needed to set up a test database seperate from the main one.
 * Not only that we need to also create a copy of app.js that doesn't have the calls to connect to a database. I do not wish to do taht thoush so I need to make the database connection elsewhere.
 */

//get the models
require('../models/Events');
require('../models/Comments');
require('../models/Posts');

mongoose.connect('mongodb://localhost/test-database');

var app = require('../debug-app');
var Event = mongoose.model('Event');

/** We want to recreate the database each time, however we cannot do this by just recalling require() to assign a new server. Not only that the code that creates the listener is not in app.js but in www file. */

function createServer() {
    server = app.listen(3000, function () {
    var port = server.address().port;
        console.log('Example app listening at port %s', port);
    });
    // done();
    return server;
}
debugger;


// //create an database instances

// //Delete database instances, before each test.

describe('Events', function(){
    //Global variables for all tests
    var server;
    describe('#01 - Get Events', function(){
        beforeEach(function (done) {
            server = createServer();
            console.log("Populating the database");
            
            var sample = new Event({
                name: "Madmen Anime Festival",
                location: "Melbourne Exhibition Center",
                startDate: new Date(2016, 9, 3),
                endDate: new Date(2016, 9, 4),
                organiser: "Madmen"
            });

            sample.save(done);

        });
        it('-> does the GET work for route \'/\' ', function getHome(done){
            request(server)
                .get('/')
                .expect(200, done);
        });
        it("-> does the GET work for route '/api/events' ", function getEvents(done){
            request(server)
                .get('/api/events')
                .expect(200, done);
        });
        it("-> does the GET work for STATIC route to /public", function getPublicStatic(done){
            request(server)
                .get('/stylesheets/style.css')
                .expect(200, done);
        });
        it("-> can you retrieve the data at '/api/events' with the correct dummy data", function getCorrectDataBack(done){
            request(server)
                .get('/api/events')
                .expect(function (res){
                    //select the data set you wish to observe
                    //in this case we want the first and only instance
                    var temp = res.body.data[0];
                    //remove the old data by creating a new object
                    res.body = new Object();
                    //pass the attributes we are only concern with
                    res.body.name = temp.name;
                    res.body.location = temp.location;
                    res.body.startDate = temp.startDate;
                    res.body.endDate = temp.endDate;
                    res.body.organiser = temp.organiser;
                })
                .expect(200, {
                    name:  "Madmen Anime Festival",
                    location: "Melbourne Exhibition Center",
                    startDate: new Date(2016, 9, 3).toISOString(),
                    endDate: new Date(2016, 9, 4).toISOString(),
                    organiser: "Madmen"
                }, done);
        });
        it("-> Can you make a POST request and use GET /api/events to retrieve the same data");
        afterEach(function (done){
            console.log("Dropping the data");
            // mongoose.connection.db.dropDatabase(done);
            // mongoose.remove equalivant to clean the database
            Event.remove({}, function (err) {
                if (err) return err;
            });
            console.log("Closing the server ...");
            server.close(done);
        });
    });
    // after(function (done){
    after(function (done){
        console.log("Dropping the database");        
        mongoose.connection.db.dropDatabase();
        mongoose.disconnect();
        mongoose.connection.close(done);
    });
});

// console.log(mongoose.connection.modelNames());
// mongoose.connect('mongodb://localhost/news', function(){
//     /* Drop the DB */
//     console.log(mongoose.connection.modelNames());
// });     

/**
 * Notes:
 * 
 * 4th Sept 2016 12 PM
 * Mysterously, calling the request multiple times within an caused errors
 * Also, there were issues with the connection to the localhost:27017 where mongoDB resided
 * Now the test works perfectly without any hiccup and now /api/events also works fine without any changes to fix the error.
 * 
 * 4th Sept 2016 1 PM
 * Turns out I was calling mongoose disconnect after the describe which I should have known would have been run because desccribe is async call .... deerp factor is real. So I was getting errors because of it. I'm not sure if keeping that code within the after of the 1st describe is good.
 * 
 * 4th Sept 2016 2 PM
 * So the order of operation in this test array is correct as I thought .
 * Runs the first describe and runs the second describe.
 * Everything inside the secound describe get run first before the first describe, then
 * the after() in the first describe is run last.
 * 
 * 4th Sept 2910 8 PM
 * There needs to be an internet connection otherwise this will fail, even if the connection is all on the localhost - which I thought wasn't necessary. Tests now all successfully work. COMPLETE
 */