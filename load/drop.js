var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/news');

// console.log("Dropping the database ...");
// console.log(mongoose.connection.modelNames());
// mongoose.connect('mongodb://localhost/news', function(){
//     /* Drop the DB */
//     mongoose.connection.db.dropDatabase();
//     console.log(mongoose.connection.modelNames());
//     mongoose.disconnect();
//     mongoose.connection.close();
// });

require('../models/Posts');
require('../models/Comments');
require('../models/Events');
require('../models/Node.js');
require('../models/NodeSerialNumbers');

var Posts = mongoose.model('Post');
var Comments = mongoose.model('Comment');
var Events = mongoose.model('Event');
var Nodes = mongoose.model('Node');
var NodeSerialNumbers = mongoose.model('NodeSerialNumber');

Posts
    .remove({ })
    .then((node) => {
        console.log(node.result);
        console.log("All Post Data removed!");
    })
    .catch((err) => {
        console.error(err);
    });

Comments
    .remove({ })
    .then((node) => {
        console.log(node.result);
        console.log("All Comment Data removed!");
    })
    .catch((err) => {
        console.error(err);
    });

// NOTES: Model.remove function
// do not use node as thinking that it gives a node object back
// While it would be nice, it return a jiberish object except the result attributte
// result: { ok: 1, n: 3 }
// where n is the number of matched objects

Nodes
    .remove({ })
    .then((node) => {
        console.log(node.result);
        console.log("All Node Data removed!");
    })
    .catch((err) => {
        console.error(err);
    });

NodeSerialNumbers
    .remove({})
    .then(function(data, err){

    }).catch(function(err){
        console.error(err)
    });


// Events.remove({}, function (err) {
//     if (err){
//         console.error(err);
//         return;
//     }    
//     console.log('Info: Deleted the event collection!');
// });

console.log('Finished drop.js');

mongoose.disconnect( () => {
    mongoose.connection.close();
})
