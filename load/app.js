// var fs = require('fs');
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
var Mixed = mongoose.Schema.Types.Mixed;

//minimum to start a connection
mongoose.connect('mongodb://localhost/api-tests');

/** SCHEMAS */
var Schema = mongoose.Schema; /// Schema Class

/**
 * title: states the topic of the blog post
 * author:
 * Date: Part of the Javascript API, Object type and hold dates
 */
var BlogSchema = new Schema({
	title:      String,
	author:     String,
	body:       String,
	comments:   [
		{ body: String, date: Date }
	],
	date:       { type: Date, default: Date.now },
	hidden:     Boolean,
	meta: {
		// votes:      Number,
		// favs:       Number,
		votes:	Number
	}
});
/**
 * Number is part Mongoose API, it defines both int and double inputs.
 */
// var ElementSchema = new Schema({
// 	name:       	String,
// 	symbol:     	{ type: String, minLength: 1, maxLength: 2 },
// 	atomicNumber: 	{ type: Number, min: 1 },
// 	state: 			String,
// 	meltingPoint: 	Number,
// 	boilingPoint: 	Number
// })

/** END of SCHEMAS **/

/** Datebase Notes: data types
 * There are 8 Basic Types that are supported; CustomeTypes can be defined or used via plugins
 * ObjectID:     Object Type by definition, called by mongoose.Schema.Types.ObjectID
 * */

// 1. Be familar with the types and how to define a schema
// 2. Call model('', schema) to get started.

/** Mongoose: Models 
 * - mongoose.model('Blog', BlogSchema) compiles a model, 'Blog' is the singual collection that references the plural collection i.e the entire Blogs collection in the database - it IMPORTANT you get this right otherwise mispelling blog will reference a new collection.
 * - IMPORTANT: make sure the schema is correct before you continue to calling model.
 * - By default every model has an associated connection (the database we connect to) hence you can call the function on its own.
 * - we can specify which databases connections our model is associated with by creating different connections and call it's model function.
 * - using mongoose is a connection; the default connection
*/

var Blog = mongoose.model('Blog', BlogSchema);

drop = false;
add = true;

if (drop){
	Blog.remove({}, function (err) {
		console.log('Clearing the Blog Collection!');
	});
}


/**In effect, what we have is basically a constructor that can create Blogs and 
 * Models eq class and instances are called documents
 * To create a document we use new b1().*/
if (add){
	// var b1 = new Blog;

	// b1.title    = 'Degas: A New Vision';   /// Expecting a String 
	// b1.author   = 'National Gallary of Victoria'; /// Expecting a String etc.
	// b1.body     = 'Part of the Wintermater piece collection, on from 25th June to the 18th Sept';
	// b1.comments  = [
	// 	{ body: 'This looks great', date: new Date(2016, 5, 29) },
	// 	{ body: 'definitely going with the FAM!', date: new Date(2016, 5, 29) },
	// 	{ body: 'DEGAS!!!!', date: new Date(2016, 5, 30) }
	// ];
	// b1.date = new Date(2016, 5, 28);
	// b1.hidden = false;
	// // b1.meta = new Object();
	// b1.meta.votes = 32578;
	// // b1.meta.favs = 281;

	// /** Call save to put this in the data base
	//  * Save can take a handler
	//  */
	// // b1.save();
	// b1.save(
	// 	function(err){
	// 	if(err){
	// 		console.log(err);
	// 	}
	// 	//Object saved!
	// });

	var b2 = new Blog({
		title: 'COGS Annual General Meeting',   /// Expecting a String 
		author: 'Yoshi', /// Expecting a String etc.
		body: 'PC Master Race, Console pesants, all welcome!',
		comments: [
			{ body: 'This looks great', date: new Date(2016, 5, 29) },
			{ body: 'Going with the FAM!', date: new Date(2016, 5, 29) },
			{ body: '(╯°□°）╯︵ ┻━┻) ', date: new Date(2016, 5, 30) }
		],
		date: new Date(2016, 5, 28),
		hidden: false,
		// b1.meta = new Object();
		meta: { votes: 32578 }
	});

	// b2.save(function(err){
	// 	if(!err){
	// 		console.log('B2 Data Saved!');
	// 	}
	// });

	var data = require('./data.json');
	//ar b3 = new Blog(data);
	var i = 0;
	for (i = 0; i < 3; ++i){
		var temp = new Blog(data[i]);
		temp.save();
		console.log("some");
	}
	//b3.save();

	// console.log(b3);
}

///Queries
/**
 * things to note:
 * arg1: is a JSON
 * arg2: is the projection or the select in sql
 * arg3: the handle to the query result
 * 
 * If do not use a callback 
 */

Blog.findOne(
	{
		'title' : 'Degas: A New Vision'
	},
	'title meta',
	function (err, Person){
		if (err) { console.log(err); return; }
		// console.log('%s %s', Person.title, Person.meta.votes);
		// if (!Person) { qill not work
		if (Person != null) {
			//%o might not be supported on terminal output rather on brower terminals
			console.log('%o', Person);
		}
		else {
			console.log('Did not find that Person');
		}
	}
);

Blog.find({}, function(err, data){
	if (data != null){
		console.log(data);
	}
})


// Put a pause function because the callback aren't being fired cause async
// mongoose.disconnect();
// mongoose.connection.close();