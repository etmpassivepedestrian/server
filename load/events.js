var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/news');

// var EventSchema = new mongoose.Schema({
// 	// id: String,
// 	name: String, //ONLY a single user
// 	location: String,
// 	startDate: Date, //Testing is needed
// 	endDate: Date, //Testing is neeeded
//     organiser: String
// });



require('../models/Events');
var Events = mongoose.model('Event');


var drop = false;

//TODO: Add the organiser attribute
//TODO: Remove the old Event data

if (!drop){
	var e1 = new Events({
		name:"Awesome Pie",
		location: "Melbourne Docklands",
		startDate: Date.now(), 
		endDate: new Date(2016, 8, 30),
		organiser: "Gracie"
	});

	var e2 = new Events({
		name:"Fundamentals",
		location: "Yarra River",
		startDate: Date.now(), 
		endDate: new Date(2016, 8, 30),
		organiser: "Tristian"
	});

	var e3 = new Events({
		name:"COGS Annual General Meeting",
		location: "La Trobe Bundoora",
		startDate: Date.now(),
		endDate: new Date(2016, 8, 30),
		organiser: "Mike"
	});

	var saveCB = function(err){
		if (err){
			console.error(err);
			return;
		}
		console.log("Event Data Saved!");
	}

	e1.save(saveCB);
	e2.save(saveCB);
	e3.save(saveCB);
}
else {
	Events.remove({}, function (err) {
		if (err){
			console.error(err);
			return;
		}
		console.log('Info: Deleted the event collection!');
	});
}
