//Test Line Comment for Git push
var express = require('express');
var router = express.Router();


var mongoose = require('mongoose');
//Grab the database models
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment'); 

/* GET home page. */
router.get('/', function(req, res, next) {
  //Will not work with HTML extension
  res.render('index');
  // res.render('index', { title: 'Express' });
});

/** OBJECT PRE-LOADING of Post */
//When express see this ':post' it will execute this first.
//Note that this code acts as preprocessing for : params

router.param('post', function(req, res, next, id) {
  //see top 
  var query = Post.findById(id);

  query.exec(function (err, post){
    if (err) { return next(err); }
    if (!post) { return next(new Error('Can\'t find post')); }

    req.post = post;
    return next();
  });
});

router.param('comment', function(req, res, next, id){
  //see top variables
  var query = Comment.findById(id);

  query.exec(function (err, comment){
    if(err){
      return next(err);
    }
    if(!comment){
      return next(new Error('Can\'t find Comment'));
    }
    //assign an attribute of comment with the data we found.
    req.comment = comment;
  })

})

// With Param we can now add this
// Because the post object was retrieved using the middleware function and attached to the req object, all our request handler has to do is return the JSON back to the client.
// router.get('/posts/:post', function(req, res) {
//   res.json(req.post);
// });

router.get('/posts/:post', function(req, res, next) {
  req.post.populate('comments', function(err, post) {
    if (err) { return next(err); }

    res.json(post);
  });
});

/** HANDLES FOR 'POSTS' DATA */

// The route for posts and also some backend to
// load the data ... retrieving the post
router.get('/posts', function(req, res, next){
  Post.find(function(err, posts){
    if(err){
      return next(err);
    }
    //We want to return all the posts
    //So all the log data from query
    res.json(posts);
  });
});

//The route for allowing new posts to be added
router.post('/posts', function(req, res, next){
  //we want to save a 'single' post
  var post = new Post(req.body);
  
  post.save(function(err, post){
    if(err) {return next(err);}
    res.json(post);
  });
});

router.put('/posts/:post/upvote', function(req, res, next) {
  req.post.upvote(function(err, post){
    if (err) { return next(err); }

    res.json(post);
  });
});

/** HANDLES FOR 'COMMNENT' DATA */

router.post('/posts/:post/comments', function(req, res, next) {
  var comment = new Comment(req.body);
  //assign the post retrieved from param to comment.post attribute
  comment.post = req.post;

  comment.save(function(err, comment){
    if(err){ return next(err); }
    //push is important, because it will add it to the end of the array
    //it is just a wrapper, remember that comments is an array and want  to putsh one
    //save will add the new data to post.

    req.post.comments.push(comment);
    req.post.save(function(err, post) {
      if(err){ return next(err); }

      res.json(comment);
    });
  });
});

router.get('/comments', function(req, res, next){
  Comment.find(function(err, data){
    if (err){
      return;
    }
    res.json(data);
  });
});

//if you cannot find the url go back to homepage
// router.get('/*', function(req, res, next) {
//   res.redirect('/');
// });


//route.get('/post/:id/comments')
//route.posts('/post/:id/comments')

// Documentation: The reason why we send JSON data as a response is that we want Angular/javascript to recieve JSON to use as Objects.

module.exports = router;
