/**
 * Code Source: https://thinkster.io/mean-stack-tutorial
 */

//Dependancies in []
var app = angular.module('flapperNews', ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {

  //wondering if the main controller still needs 
  //to be specified on the html body tag 
  // $stateProvider.state('home',{
  //     url: '/home',
  //     templateUrl: '/home.html',
  //     controller: 'MainCtrl'
  //   });
  
  // $stateProvider.state('posts', {
  //   url: '/posts/{id}',
  //   templateUrl: '/posts.html',
  //   controller: 'PostsCtrl'
  // });

  $stateProvider.state('home', {
    url: '/home',
    templateUrl: '/home.html',
    controller: 'MainCtrl',
    //removing $http from config broke angular
    resolve: {
      postPromise: ['posts', function(posts){
        return posts.getAll();
      }]
    }
  });

  $stateProvider.state('posts', {
    url: '/posts/{id}',
    templateUrl: '/posts.html',
    controller: 'PostsCtrl',
    resolve: {
      post: ['$stateParams', 'posts', function($stateParams, posts) {
        return posts.get($stateParams.id);
      }]
    }
  });

  $urlRouterProvider.otherwise('home');
}]);

//using factories are the eq of services
//The ideas is that this object and its data is avaliable to controllers
//Persistant data.
//https://tylermcginnis.com/angularjs-factory-vs-service-vs-provider-5f426cfe6b8c#.caxzbvvu2
app.factory('posts', ['$http', function($http){
  //service body

  var o = 
  {
    posts: []
  };

  o.getAll = function() {
    return $http.get('/posts').success(function(data){
      angular.copy(data, o.posts);
    });
  };

  o.create = function(post) {
    return $http.post('/posts', post).success(function(data){
      o.posts.push(data);
    });
  };
  
    //This call to server will update the database
    //If it is sucessful it will update the current post data to the user.
  o.upvote = function(post) {
    return $http.put('/posts/' + post._id + '/upvote')
      .success(function(data){
        post.upvotes += 1;
      });
  };

  o.get = function(id) {
    return $http.get('/posts/' + id).then(function(res){
      return res.data;
    });
  };

  o.addComment = function(id, comment) {
    return $http.post('/posts/' + id + '/comments', comment);
  };
  // //This is then called when addComment within the Post Contoller
  // //The pattern is that you have functions which are used in the controller; hence two part add function and use it in the controller

  // o.upvoteComment = function(post, comment) {
  //   return $http.put('/posts/' + post._id + '/comments/'+ comment._id + '/upvote')
  //     .success(function(data){
  //       comment.upvotes += 1;
  //     });
  // };
  return o;
}])
app.controller('PostsCtrl', [
'$scope',
//'$stateParams', //Why does this throw an ERROR if it is included?
'posts',
'post', //my single post from query
// function($scope, $stateParams, posts){
//   $scope.post = posts.posts[$stateParams.id];
function($scope, posts, post){
  $scope.post = post;
  //See the form within 'post' template 

  // $scope.addComment = function(){
  //   //if nothing has been provided, ignore it
  //   if($scope.body === '') { return; }
  //   //otherwise, add the comment
  //   $scope.post.comments.push({
  //     body: $scope.body,
  //     author: ($scope.user == null  ? 'anonymous' : $scope.user),//'user',
  //     upvotes: 0
  //   });
  //   $scope.body = '';
  //   $scope.user = null;
  // };

  //Difference between the 
  $scope.addComment = function(){
    if($scope.body === '') { return; }
    posts.addComment(post._id, {
      body: $scope.body,
      author: 'user',
    }).success(function(comment) {
      $scope.post.comments.push(comment);
    });
    $scope.body = '';
  };

  $scope.incrementUpvotes = function(comment){
    posts.upvoteComment(post, comment);
  };
}]);

app.controller('MainCtrl', [
'$scope',
'posts',
function($scope, posts){

  /* 
   * Here Main Control defines post data 
   */
  $scope.posts = posts.posts;
  $scope.title = 'Hello There';

  $scope.addPost = function(){
    if(!$scope.title || $scope.title === '') { return; }
    posts.create({
      title: $scope.title,
      link: $scope.link,
    });
    $scope.title = '';
    $scope.link = '';
  };
  /** This is another function, one that increments the likes
   * on a post.
  */
  // $scope.incrementUpvotes = function(post){
  //   post.upvotes += 1;
  // }
  //Replaced with this
  $scope.incrementUpvotes = function(post) {
    posts.upvote(post);
  };
}]);