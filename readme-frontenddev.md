## Server Bulid requirements for Front End Developers

### Placing your webpages and its resourcces 

If you have stylesheets, javascript, images that are used by your webpages. Put them within the `public` directory of the solution into appropriate sections.

HTML documents do within the views as .ejs files not .html.

Don't forget to update the reference within the html documents, relative the directory structure.

i.e angularApp.js is refernced by index.html as "../public/javascripts/angularApp.js"

That's it!

### Running Scripts

If you have scripts or programs that need to be run. Place them within `bin/` directory.

Author: Daniel Minero
Last Updated by: Daniel Minero 
Last Revision: August 6th 2016, 12:32PM


