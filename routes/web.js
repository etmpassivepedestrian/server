/*
* This is our current web API.
* it includes access request to the database.
*/

var express = require('express');
var rOptions = {
	caseSensitive: true
}
var router = express.Router(rOptions);

//Grab our database models
var mongoose = require('mongoose');
var Events = mongoose.model('Event');
var Nodes = mongoose.model('Node');


//GET all event data

//This is the NEW code to read event data and pass id instead of _id
router.get('/events', function(req, res, next) {
	var temp = [];
	//Query the Events base
	Events.find().then( (data) => {
		data.forEach( (value, index) => {
			var o = {
				id: value._id.toString(),
				name: value.name,
				location: value.location,
				startDate: value.startDate,
				endDate: value.endDate,
				organiser: value.organiser,
				nodesAttached: value.nodesAttached
			};
			temp.push(o);
		});
		
		// console.log(temp);
		// res.json(temp);
		var body = new Object();
		body.data = temp;
		res.data = body;
	}).then( () => {
		next();
	}).catch( (err) => {
		next(err);
	});
}).get('/events', function(req, res, next){
	// console.log("Secound Event Call");
	res.json(res.data);
	// console.log('Format: \n', res.data);
});

// We can add the event that we find to req object otherwise
// return an error 
router.param('event', function(req, res, next, id){
	//@ :event id obtains the string at that param
	var query = Event.findById(id);
	query.exec(function (err, event){
		if(err){
			return next(err); 
		}
		if(!event){
			return next(new Error('Can\'t find the specified Event'));
		}
		//else, add the data we found to request object
		req.event = event;
	})
});

//GET specific event data
router.get('/events/:event', function(req, res, next){
	//return all the event data currently in the database
	//Obviously this wil need to change if there are multiple
	if (!req.event){
		Console.err("We could not find the event requested!");
		return;
	}
});

/** Logging date string to the console, if needed to view differences */
function logDate(msg, date, offset){
	console.log(msg);
	console.log(date.toString());
	console.log(date.toUTCString());
	console.log(date.toISOString());
	console.log(date.toLocaleString());
	console.log(date.getTime());
	console.log(offset / 60.0);
}
/** Changes the date to normalize the timezone to 0 using the getTimezoneOffset */
function normalise(date, offset){
	// logDate("Original Date:", date, offset);
	var temp = new Date(date.getTime() + (offset * 60000));
	// logDate("Adjusted Timeoffset:", date, offset);
	return temp;
}

router.post('/events', function(req, res, next){
	console.log('Output : Body >>');
	console.log(req.body);
	//console.log(req.headers.offset);
	var os = parseInt(req.headers.offset);
	var event = new Events(req.body); 
	//Normalized the date fields by subtracting the offset
	event.startDate = normalise(event.startDate, os);
	event.endDate = normalise(event.endDate, os);

	event.save(function(err, event){
		if(err) {return next(err);}
		res.json(event);
		
		console.log('Output : Event Document >>');
		console.log(event);
	});
});

router.get('/nodes', function(req, res, next){
	// Query the node database store
	// Using query's lean function to obtain a Javascript object instead of a mongoose document object
	var query = Nodes.find().lean().exec(function(err, data){
		if(err){
			console.err(err);
			return next(err);
		}
		//Another Angular2 Hackjob
		var body = new Object();
		body.data = data;
		body.data.forEach((value, index) => {
			value._id = value._id.toString();
			// console.log(value);
			value.id = value._id;
			delete value._id;
		}, this);

		res.json(body);
		console.log('Format: \n', data);
	});
});

router.post('/nodes', function(req, res, next){
	console.log('Output : Body >>');
	console.log(req.body);

	var node = new Nodes(req.body);

	node.save(function(err, node){
		if(err) {return next(err);}
		res.json(node);
		
		console.log('Node Document >>');
		console.log(node);
	});
});

module.exports = router;

 
/**
Links:
http://expressjs.com/en/4x/api.html#express.router

Todo:
Connect stuff together
	[X] -- Add Event.js to app run
	[X] -- Create Schema for Event
*/
