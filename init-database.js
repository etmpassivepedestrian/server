/** Code need to be before routes */
var mongoose = require('mongoose');
// I believe it works same as inlining js code that is inside post.js

//I'll assume that we set a host other than hostname like my own url 'acreativemind.sytes.net'
mongoose.connect('mongodb://localhost/news');

require('./models/Posts');
require('./models/Comments');
require('./models/Events');
require('./models/Node');
// require('./models/NodeSerialNumbers');
// require('./models/Elements');

// Add bluebird promise library
mongoose.Promise = require('bluebird');

// mongoose.connect('mongodb://localhost/localNews');
module.exports = mongoose;