// var fs = require('fs');
// var path = require('path');
var _ = require('lodash');

var express = require('express');
var routerOptions = {
    caseSensitive: true
}
var router = express.Router(routerOptions);

/** DATABASE MODELS */

var mongoose = require('mongoose');
var Posts = mongoose.model('Post');
var Events = mongoose.model('Event');
var Nodes = mongoose.model('Node');

router.param('id', function(req, res, next, id){
    var o = new Object();
    o.id = id;
    res.params = o;
    next();
});

function logDate(msg, date, offset){
	console.log(msg);
	console.log(date.toString());
	console.log(date.toUTCString());
	console.log(date.toISOString());
	console.log(date.toLocaleString());
	console.log(date.getTime());
	console.log(offset/60.0);
}
/** Changes the date to normalize the timezone to 0 using the getTimezoneOffset */
function normalise(date, offset){
	// logDate("Original Date:", date, offset);
	var temp = new Date(date.getTime() + (offset * 60000));
	// logDate("Adjusted Timeoffset:", date, offset);
	return temp;
}

//update in the body
//this update will also include the adding of node ids
router.put('/events/:id', function(req, res, next){
    // There is a req.params.id
    // console.log('With content:\n', res.params.data);

    //Filters the incoming data so that empty field do not get updated.
    console.log(req.body);
    console.log(req.body.nodesAttached);
    //console.log(req.headers.offset);
    var o = new Object();
    var os = parseInt(req.headers.offset);
    //if (!_.eq('', req.body.name) || req.body.name === undefined)
    if (req.body.name)
        o.name = req.body.name;
    if (req.body.location)
        o.location = req.body.location;
    if (req.body.startDate)
        o.startDate = normalise(new Date(req.body.startDate), os);
    if (req.body.endDate)
	    o.endDate = normalise(new Date(req.body.endDate), os);
    if (req.body.organiser)
        o.organiser = req.body.organiser;
    if (req.body.nodesAttached){
        o.nodesAttached = [];
        if (req.body.nodesAttached instanceof Array){
            o.nodesAttached = req.body.nodesAttached;
        }
        else {//if (req.body.nodesAttached instanceof String){
            o.nodesAttached.push(req.body.nodesAttached);
        }
        //check for node data in the database
        o.nodesAttached.forEach( (value, index) => {
            // change
            var query = Nodes.findById(value);
            query.exec().then((node) => {
                if (node){
                    console.log("Node %s Found!", value);
                }
            }).catch((err) => {
                console.error(err);
                next(err);
            });
        }, this);
    }


    //What if they do not specify the id again when they make this request?
    var id = req.params.id;
    console.log('To Update Document: (id) ', req.params.id);
    Events.findOneAndUpdate({ _id: id}, o, { new: true })
    .then( (data) => {
        console.log(o);
        // console.log(req.body);
        /** Logging date string to the console, if needed to view differences */
        console.log(data);
        if (!data)
            return res.status(404).render('error', { message: 'Document not found!', error: new Error('Id did not match any document')});
        res.send('Document ' + id + ' has been updated!');
    }).catch( (err) => {
        next(err);
    });
});

router.get('/events/:id/addNodesToEventList', function(req, res, next) {
    var event_id = req.params.id;
    var attached_nodes = [];

    Events.findById({ _id: event_id }).lean().then( (data) => {
        console.log('%d Nodes attached', data.nodesAttached.length);
        attached_nodes = data.nodesAttached;
    }).then(() => {
        console.log(attached_nodes);

        var query = Nodes.find({});
        attached_nodes.forEach( (value, index) => {
            console.log('id: %s', value);
            query = query.where('_id').ne(value);
        }, this);

        query.lean().exec()
        .then( (data) => res.json(data))
        .catch( (err) => {
            console.error(err);
            next(err);
        });
    }).catch( (err) => {
        console.error(err);
        next(err);
    });
});
router.delete('/events/:id', function (req, res, next) {
    //code
    var id = req.params.id;
    Events.remove({ _id: id }).then( (result) => {
        if (result.n != 0){
            console.log("Event found and removed!");
            var send = "Event " + id + " removed from the database!";
            res.send(send);
        }
        else {
            res.send("Event document does not exist in the database!");
        }
    }).catch((err) => {
        console.error(err);
        next(err);
    });
});

// router.params('node', function(req, res, next){
    
// });

router.delete('/events/:event/nodesAttached/:node', function (req, res, next) {
    //check if the associated node is in the list
    var event = req.params.event;
    var node = req.params.node;
    Events.findById(event).then( (eventData) => {
        if(!eventData) {
            res.send("Event Document Not Found!");
            return;
        }
        var index = eventData.nodesAttached.indexOf(node);
        if(index != -1){
            eventData.nodesAttached.pull({ _id: node});  
            eventData.save(function(){
                console.log("Event Updated!");
            });              
            res.send("Dropped node " + node);
        }
        else {
            res.status(404).send("Node already detached from Event " + event);
        }        
    }).catch((err) => {
        console.error(err);
        next(err);
    }); 
});

//update in the query
// router.put('/events/:id', function(req, res, next){
//     var id = res.params.id;
//     var o = {
//         name: req.query.name,
//         location: req.query.location,
//         startDate: req.query.startDate,
//         endDate: req.query.endDate,
//         organiser: req.query.organiser
//     }
//     console.log(o);
//     res.send('Document ' + id + ' has been updated!');
//     // Events.findOneAndUpdate()
// });
router.put('/nodes', function(){
    return next(new Error("Not Implemented!"));
}); 

module.exports = router;
