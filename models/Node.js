var mongoose = require('mongoose');
// var promises = require('bluebird');

var NodeSchema = new mongoose.Schema({
	serial: String, //Assigned to the pi device
	// serial: { type: String, unique: true } //Assigned to the pi device
	checked: { type: Boolean, default: false }
});

mongoose.model('Node', NodeSchema);


