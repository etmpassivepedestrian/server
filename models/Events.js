var mongoose = require('mongoose');

// http://mongoosejs.com/docs/guide.html
// paletton.com thats web colours ignore that

var EventSchema = new mongoose.Schema({
	// id: { type: String, unique: true },
	name: String, //ONLY a single user
	location: String,
	startDate: Date, //Testing is needed
	endDate: Date, //Testing is neeeded
    organiser: String,
	nodesAttached: [
		{ type: mongoose.Schema.Types.ObjectId, ref: 'Node' }
	]
	// nodesAttached: [
	// 	{ type: String, ref: 'Node' }
	// ]	
});

//Now we need to do test/queries on this new schema 

mongoose.model('Event', EventSchema);

//Date objects
//PART of the javascript
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
//Using Date.parse(String)
//also new Date(year, month[, day[, hour,[...]]);

//Evan's Rubiks Cube Times
//1:09
//0:58:73

